package com;

public class GarbageCollectorDemo {
	
	@Override
	protected void finalize() throws Throwable {
		System.out.println("I am garbage collected");
	}

	
	public static void main(String[] args) {
		
		GarbageCollectorDemo gd = new GarbageCollectorDemo();
		gd=null;
		String str= null;
		
		System.gc();
	}
}
